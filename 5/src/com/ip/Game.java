import java.util.ArrayList;

import static java.lang.System.exit;

/**
 * Created by Razvan-PC on 3/23/2017.
 */
public class Game {

    public Player player;
    public ArrayList<Level> levels;
    public Level currentLevel;

    private static Game ourInstance;

    public static Game getInstance()
    {
        return ourInstance;
    }

    public void createGame(Player player, ArrayList<Level> levels, Level currentLevel) {
        if (ourInstance == null)
        {
            ourInstance=new Game(player, levels,currentLevel);
        }

    }

    public void start() {
        System.out.println("Game has started");
    }

    public void stop() {
        exit(0);
    }

    public void tryAgain() {
        System.out.println("Restarting the level");
    }


    private Game(Player player, ArrayList<Level> levels, Level currentLevel) {
        this.player = player;
        this.levels = levels;
        this.currentLevel = currentLevel;
    }


    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public ArrayList<Level> getLevels() {
        return levels;
    }

    public void setLevels(ArrayList<Level> levels) {
        this.levels = levels;
    }

    public Level getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(Level currentLevel) {
        this.currentLevel = currentLevel;
    }
}

