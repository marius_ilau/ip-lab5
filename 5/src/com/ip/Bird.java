import java.util.Vector;

public class Bird {

  public Double weight;

  public Double size;
  public Sling mySling;

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  public Double getSize() {
    return size;
  }

  public void setSize(Double size) {
    this.size = size;
  }

  public Sling getMySling() {
    return mySling;
  }

  public void setMySling(Sling mySling) {
    this.mySling = mySling;
  }

  public Bird(Double weight, Double size, Sling mySling) {

    this.weight = weight;
    this.size = size;
    this.mySling = mySling;
  }
}