import java.util.Vector;

/**
 * This is the main class for the player
 */
public class Player {

    public Double score;

    public Integer rank;

    public String username;

    public Sling sling;


    public Integer newAttr;

    public Sling mySling;
    public Vector myLevel;
    public Vector myGame;

    private static Player ourInstance = new Player();

    public static Player getInstance() {
        return ourInstance;
    }

    private Player() {
    }

    
   // public String getUsername() {}

    public Double getScor() {
        return this.score;
    }

    public Sling getSling() {
        return this.sling;
    }

    public void newOperation() {
        //do operations
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (score != null ? !score.equals(player.score) : player.score != null) return false;
        if (rank != null ? !rank.equals(player.rank) : player.rank != null) return false;
        if (username != null ? !username.equals(player.username) : player.username != null) return false;
        if (sling != null ? !sling.equals(player.sling) : player.sling != null) return false;
        if (newAttr != null ? !newAttr.equals(player.newAttr) : player.newAttr != null) return false;
        if (newAttr != null ? !newAttr.equals(player.newAttr) : player.newAttr != null) return false;
        if (mySling != null ? !mySling.equals(player.mySling) : player.mySling != null) return false;
        if (mySling != null ? !mySling.equals(player.mySling) : player.mySling != null) return false;
        if (myLevel != null ? !myLevel.equals(player.myLevel) : player.myLevel != null) return false;
        if (myGame != null ? !myGame.equals(player.myGame) : player.myGame != null) return false;
        return myGame != null ? myGame.equals(player.myGame) : player.myGame == null;
    }

    @Override
    public String toString() {
        return "Player{" +
                "score=" + score +
                ", rank=" + rank +
                ", username='" + username + '\'' +
                ", sling=" + sling +
                ", newAttr=" + newAttr +
                ", newAttr=" + newAttr +
                ", mySling=" + mySling +
                ", mySling=" + mySling +
                ", myLevel=" + myLevel +
                ", myGame=" + myGame +
                ", myGame=" + myGame +
                '}';
    }

    @Override
    public int hashCode() {
        int result = score != null ? score.hashCode() : 0;
        result = 31 * result + (rank != null ? rank.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (sling != null ? sling.hashCode() : 0);
        result = 31 * result + (newAttr != null ? newAttr.hashCode() : 0);
        result = 31 * result + (newAttr != null ? newAttr.hashCode() : 0);
        result = 31 * result + (mySling != null ? mySling.hashCode() : 0);
        result = 31 * result + (mySling != null ? mySling.hashCode() : 0);
        result = 31 * result + (myLevel != null ? myLevel.hashCode() : 0);
        result = 31 * result + (myGame != null ? myGame.hashCode() : 0);
        result = 31 * result + (myGame != null ? myGame.hashCode() : 0);
        return result;
    }
}