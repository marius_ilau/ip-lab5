import java.util.ArrayList;
import java.util.Vector;

public class Level {

  public java.util.Vector birds;

  public Boolean loked;

  public Integer noStars;

  public Double passingScore;

  public Double score;

  public ArrayList<Obstacle> obstacle;

  public Game myGame;
  public Player myPlayer;
    /**
   * 
   * @element-type Obstacle
   */

  public Level reset() {
    System.out.println("Reseting the level");

    return new Level();
  }

  public Level() {
    this.score = Double.valueOf(0);
    this.obstacle = new ArrayList<Obstacle>();

    Obstacle firstObstacle = new Obstacle();
    Obstacle secondObstacle = new Obstacle();
    Obstacle thirdObstacle = new Obstacle();

    this.obstacle.add(firstObstacle);
    this.obstacle.add(secondObstacle);
    this.obstacle.add(thirdObstacle);

    obstacle.forEach(item -> {
        item.setSize(Double.valueOf(5));
        item.hp = Double.valueOf(100);
    });
  }

  public Double getScore() {
    return score;
  }

  public Integer computeStars()
  {
    Double totalScore = 0.0;
    for (Obstacle ob: this.obstacle) {
      totalScore += ob.hp;
    }
    Double myRange = this.getScore()/totalScore;
    if(myRange > 0.9)
      return 3;
    else if (myRange < 0.9 && myRange > 0.75)
      return 2;
    else if (myRange < 0.75 && myRange > 0.5)
      return 1;
    else return 0;
  }
}