import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by stefanstolniceanu on 3/30/17.
 */
class LevelTest {
    private Level pristineLevel;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        this.pristineLevel = new Level();
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {

    }

    @org.junit.jupiter.api.Test
    void reset() {
        Level resetTest;

        resetTest = pristineLevel.reset();

        assertEquals(resetTest.obstacle.size(), pristineLevel.obstacle.size());
        assertEquals(resetTest.getScore(), pristineLevel.getScore());
    }



    @org.junit.jupiter.api.Test
    void getScore() {
        Double staticScore = Double.valueOf(5);

        pristineLevel.score = staticScore;
        assertEquals(pristineLevel.getScore(), staticScore);
    }

    @org.junit.jupiter.api.Test
    void computeStars3()
    {
        pristineLevel.score = Double.valueOf(300);

        assertEquals(Math.toIntExact(pristineLevel.computeStars()), 3);
    }

    @org.junit.jupiter.api.Test
    void computeStars2()
    {
        pristineLevel.score = Double.valueOf(240);

        assertEquals(Math.toIntExact(pristineLevel.computeStars()), 2);
    }

    @org.junit.jupiter.api.Test
    void computeStars1()
    {
        pristineLevel.score = Double.valueOf(200);

        assertEquals(Math.toIntExact(pristineLevel.computeStars()), 1);
    }



}