import java.util.Vector;


public class Obstacle {

    public Double size;

    public Double hp;

    public Integer newAttr;

    public Level myLevel;
    public Vector myWood;
    public Vector myPig;

    public Double getSize() {
        return this.size;
    }

    public void setSize(Double size) {
        this.size = size;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Obstacle obstacle = (Obstacle) o;

        if (size != null ? !size.equals(obstacle.size) : obstacle.size != null) return false;
        if (hp != null ? !hp.equals(obstacle.hp) : obstacle.hp != null) return false;
        if (newAttr != null ? !newAttr.equals(obstacle.newAttr) : obstacle.newAttr != null) return false;
        if (myLevel != null ? !myLevel.equals(obstacle.myLevel) : obstacle.myLevel != null) return false;
        if (myWood != null ? !myWood.equals(obstacle.myWood) : obstacle.myWood != null) return false;
        return myPig != null ? myPig.equals(obstacle.myPig) : obstacle.myPig == null;
    }

    @Override
    public int hashCode() {
        int result = size != null ? size.hashCode() : 0;
        result = 31 * result + (hp != null ? hp.hashCode() : 0);
        result = 31 * result + (newAttr != null ? newAttr.hashCode() : 0);
        result = 31 * result + (myLevel != null ? myLevel.hashCode() : 0);
        result = 31 * result + (myWood != null ? myWood.hashCode() : 0);
        result = 31 * result + (myPig != null ? myPig.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Obstacle{" +
                "size=" + size +
                ", hp=" + hp +
                ", newAttr=" + newAttr +
                ", myLevel=" + myLevel +
                ", myWood=" + myWood +
                ", myPig=" + myPig +
                '}';
    }
}