import java.util.Vector;


/**
 * This is the mockup class for the pigs that need to be killed
 * in the game by slinging the birst towards them;
 */

public class Pig extends Obstacle {

    public Boolean state;

    public Vector myObstacle;

    public void die() {
        // method to explode pig
        System.out.println("Pig has died.");
    }

}